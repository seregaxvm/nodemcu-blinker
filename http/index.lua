local function sendOk(connection)
   connection:send("HTTP/1.1 301 MOVED PERMANENTLY\r\nContent-Type: text/html; charset=utf-8\r\nLocation: /\r\nContent-Length: 0\r\n\r\n")
end

return function (connection, req, args)

   -- Make this work with both GET and POST methods.
   -- In the POST case, we need to extract the arguments.
   print("method is " .. req.method)
   if req.method == "POST" then
      local rd = req.getRequestData()
      for name, value in pairs(rd) do
         args[name] = value
      end
   end

   -- perform action

   local pin = 8
   
   if args.action == "on" then
      gpio.write(pin, gpio.HIGH)
      sendOk(connection)
      return
   end

   if args.action == "off" then
      gpio.write(pin, gpio.LOW)
      sendOk(connection)
      return
   end

   -- everything else is error
   sendOk(connection)

end
